import React, { Component } from 'react'
import { Text, Dimensions,Image,View,ActivityIndicator ,StyleSheet } from 'react-native'

import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';

const {width,height} = Dimensions.get('window');

export class Test extends Component {
  

  constructor(){
    super()
    this.state = {
      isLoading: true,
      dataSource: {}
    }
  }

  componentDidMount()
    {
      var item = this.props.id;
      fetch(`http://www.gobi.mn/api/hehe/${item}`).then((response) => response.json())
      .then((responseJson) => {
        //  let MyObject = Object.keys(responseJson).length;
        //  alert(MyObject);
        this.setState({
          dataSource: responseJson.products,
          isLoading: false
        });

      })
    }
    render() {
      const {dataSource} = this.state;
      if(this.state.isLoading){
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" animating />
        </View>
      )
  
     } else {
       return(
        // <View style={styles.container}>
        //     {/* <Text>{JSON.stringify(this.state.dataSource)}</Text> */}
        //     <Image 
        //     source={{uri: 'http://www.gobi.mn/storage/' + dataSource.pics}}
        //     style={{width: width -1, height: 400}}
        //     />
        //     <Text>{dataSource.name}</Text>
        // </View>
        <Container>
        <Content>
          <Card>

            <CardItem cardBody>
            <Image 
            source={{uri: 'http://www.gobi.mn/storage/' + dataSource.pics}}
            style={{width: width -5, height: 400}}
            />
            </CardItem>
            <CardItem>
              <Left><Text>name: {dataSource.name}</Text></Left>
            </CardItem>
            <CardItem>
              <Left><Text>color: {dataSource.color}</Text></Left>
            </CardItem>
            <CardItem>
              <Left><Text>style: {dataSource.style_no}</Text></Left>
            </CardItem>
            <CardItem>
              <Left><Text>description: {dataSource.style_no}</Text></Left>
            </CardItem>
          </Card>
        </Content>
      </Container>
        );
     }
    }
}

export default Test

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      // justifyContent: 'center',
      alignItems: 'center'
  },
  item: {
    padding: 3,
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    
  },
  activity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

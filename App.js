/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome'
import {Router,Stack,Actions,Scene} from 'react-native-router-flux'

import Index from './BottomTab/Index'
import Product from './BottomTab/Product'
import Settings from './BottomTab/Settings'

import Now from './Now/Now'
import NowItem from './Now/NowItem'

import Product_item from './Gobi/Product_item'
import Gobi from './Brand/Gobi'
import GobiEmPalito from './Gobi/GobiEmPalito'


const TabIcon = ({focused, iconName}) => {
  var color = focused ? '#34495e' : '#7f8c8d';
    return(
      <Icon name={iconName} color={color} size={25} />
    )
};

class App extends React.Component {

  render() {
    return(
      <Router>
        <Scene key="root">
          <Scene key="tabbar"
          showLabel={false}
          tabs
          hideNavBar>
  {/* Tab 1 */}
            <Scene
              key="tab1"
              icon={TabIcon}
              iconName="home"
              
              >
                <Scene
                  key="Index"
                  component={Index} />
              </Scene>
  {/* Tab 2 */}
              <Scene
                key="tab2"
                icon={TabIcon}
                iconName="shopping-bag">
                  <Scene
                    key="Product"
                    component={Product} />

                  <Scene
                    key="Gobi"
                    component={Gobi} />
                  
                  <Scene
                    key="GobiEmPalito"
                    component={GobiEmPalito} />

                  <Scene
                    key="Product_item"
                    component={Product_item} />
                </Scene>
  {/* Tab 3 */}
              <Scene
                key="tab3"
                icon={TabIcon}
                iconName="bandcamp">
                
              <Scene
              key="Now"
              title="Now News"
              component={Now} />

              <Scene
              key="NowItem"
              title="Now Item"
              component={NowItem} />

              </Scene>
  {/* Tab 4 */}
              <Scene
                key="tab4"
                icon={TabIcon}
                iconName="user">
                  <Scene
                    key="NowItem"
                    title="Settings"
                    component={Settings} />
                </Scene>



          </Scene>
        </Scene>
      </Router>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

import React, { Component } from 'react'
import { Text, Dimensions,Image,StyleSheet,TouchableOpacity,ActivityIndicator,FlatList, View } from 'react-native'

import {Actions} from 'react-native-router-flux'

const numColumns = 1;
var width = Dimensions.get('window').width - 6; 
export default class Index extends Component {

    constructor(){
      super()
      this.state = {
        isLoading: true,
        dataSource: []
      }
    }

    componentDidMount()
    {
      var item = this.props.id;
      fetch('http://www.gobi.mn/api/now').then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          dataSource: responseJson.now
        })
      })
      // Actions.Flex()}
    }
    _renderItem = ({item}) => (
      <TouchableOpacity onPress={ this.onPressShow.bind(this,item.now_id) }>
        <View style={styles.item}>
        <Image 
            source={{uri: 'http://www.gobi.mn/storage/' + item.pics}}
            style={{width: width, height: 206,borderRadius: 3,}}
        />
        </View>
      </TouchableOpacity>
    );

    onPressShow(item) {
      Actions.NowItem({id: item });
      // alert(item);
  }

  render() {
    console.disableYellowBox = true; 
    if(this.state.isLoading){
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" animating />
      </View>
    )

   } else {
     return(
      <View style={styles.container}>
      
          <FlatList data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index}
            numColumns={numColumns} style={{marginTop: 3,flex: 1,}}
      />
      </View>
      );
   }
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
      marginBottom: 3,
    }
});

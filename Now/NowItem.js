import React, { Component } from 'react'
import { Text, Dimensions,Image,View,ActivityIndicator ,StyleSheet } from 'react-native'

import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import HTML from 'react-native-render-html';
const {width,height} = Dimensions.get('window');

export class NowItem extends Component {
  

  constructor(){
    super()
    this.state = {
      isLoading: true,
      dataSource: {}
    }
  }

  componentDidMount()
    {
      var item = this.props.id;
      fetch(`http://www.gobi.mn/api/now_item/${item}`).then((response) => response.json())
      .then((responseJson) => {
        //  let MyObject = Object.keys(responseJson).length;
        //  alert(MyObject);
        this.setState({
          dataSource: responseJson.now,
          isLoading: false

        });

      })
    }
    render() {
      console.disableYellowBox = true; 
      const {dataSource} = this.state;
      if(this.state.isLoading){
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" animating />
        </View>
      )
      // <Text>{JSON.stringify(this.state.dataSource)}</Text>
     } else {
       return(
      <Container>
        <Content>
          <Card>

            <CardItem cardBody>
            <Image 
            source={{uri: 'http://www.gobi.mn/storage/' + dataSource.pics}}
            style={{width: width -5, height: 250}}
            />
            </CardItem>
            <CardItem>
              <Body><Text>{dataSource.title}</Text></Body>
            </CardItem>
            <CardItem>
              <Left>
                <HTML html={dataSource.desc} />
              </Left>
            </CardItem>
          </Card>
        </Content>
      </Container>
        );
     }
    }
}

export default NowItem

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      // justifyContent: 'center',
      alignItems: 'center'
  },
  item: {
    padding: 3,
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    
  },
  activity: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
